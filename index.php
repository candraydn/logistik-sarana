<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A layout example that shows off a blog page with a list of posts.">
    <title>Logistik Sarana</title>

    <link rel="stylesheet" href="assets/css/pure-min.css">
    <link rel="stylesheet" href="assets/css/pure-responsive-min.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
    <div id="main" class="pure-g">
        <div class="sidebar pure-u-1 pure-u-md-1-2">
            <div class="header-large">
                <h1>Sistem Logistik Sarana</h1>
                <h2>RS. Orthopaedi Purwokerto</h2>

                <nav class="nav">
                    <ul>
                        <li>
                            <a class="pure-button active" href="#">Login</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>

        <div class="content pure-u-1 pure-u-md-1-2">
            <div class="header-medium">

                <div class="items">
                    <h1 class="subhead">Login</h1>
                  <?php
                  session_start();

                  require_once "require/Auth.php";
                  require_once "require/Util.php";

                  $auth = new Auth();
                  $db_handle = new DBController();
                  $util = new Util();

                  require_once "require/authCookieSessionValidate.php";

                  if ($isLoggedIn) {
                      $util->redirect("dashboard.php");
                  }

                  if (! empty($_POST["login"])) {
                      $isAuthenticated = false;

                      $username = $_POST["member_name"];
                      $password = $_POST["member_password"];

                      $user = $auth->getMemberByUsername($username);
                      if (password_verify($password, $user[0]["member_password"])) {
                          $isAuthenticated = true;
                      }

                      if ($isAuthenticated) {
                          $_SESSION["member_id"] = $user[0]["member_id"];

                          // Set Auth Cookies if 'Remember Me' checked
                          if (! empty($_POST["remember"])) {
                              setcookie("member_login", $username, $cookie_expiration_time);

                              $random_password = $util->getToken(16);
                              setcookie("random_password", $random_password, $cookie_expiration_time);

                              $random_selector = $util->getToken(32);
                              setcookie("random_selector", $random_selector, $cookie_expiration_time);

                              $random_password_hash = password_hash($random_password, PASSWORD_DEFAULT);
                              $random_selector_hash = password_hash($random_selector, PASSWORD_DEFAULT);

                              $expiry_date = date("Y-m-d H:i:s", $cookie_expiration_time);

                              // mark existing token as expired
                              $userToken = $auth->getTokenByUsername($username, 0);
                              if (! empty($userToken[0]["id"])) {
                                  $auth->markAsExpired($userToken[0]["id"]);
                              }
                              // Insert new token
                              $auth->insertToken($username, $random_password_hash, $random_selector_hash, $expiry_date);
                          } else {
                              $util->clearAuthCookie();
                          }
                          $util->redirect("dashboard.php");
                      } else {
                          $message = "Invalid Login";
                      }
                  }
                  ?>
                  <?php if(isset($message)) {
                    echo "<aside class='pure-message message-error'>
                        <p><strong>ERROR</strong>: Password SALAH.</p>
                    </aside>"; } ?>

                    <form action="" method="post" id="frmLogin" class="pure-form pure-form-stacked">
                        <fieldset>

                            <label for="email">Username</label>
                            <input id="email" type="Text" name="member_name" placeholder="Username" class="pure-input-1"
                            value="<?php if(isset($_COOKIE["member_login"])) { echo $_COOKIE["member_login"]; } ?>">

                            <label for="password">Password</label>
                            <input id="password" type="password" name="member_password" placeholder="Password" class="pure-input-1"
                            value="<?php if(isset($_COOKIE["member_password"])) { echo $_COOKIE["member_password"]; } ?>">

                            <label for="remember" class="pure-checkbox">
                              <input type="checkbox" name="remember" id="remember"
                              <?php if(isset($_COOKIE["member_login"])) { ?> checked
                              <?php } ?> /> Remember me
                            </label>

                            <input type="submit" name="login" value="Login" class="pure-button button-success">
                        </fieldset>
                    </form>
                </div>

                <?php require_once "footer.php"; ?>
