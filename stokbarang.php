<?php
require_once "header.php";

function rupiah($angka){
	
	$hasil_rupiah = "Rp " . number_format($angka,0,',','.');
	return $hasil_rupiah;
 
}
 ?>
<div class="content pure-u-1 pure-u-md-21-24">
    <div class="header-small">

        <div class="items">
            <h1 class="subhead">Stok Barang <a href="require/modulProduk/tambahProduk.php" class="pure-button button-small button-success">Tambah
                    Barang</a></h1>
        </div>
        <div class="pure-g">
            <div class="pure-u-1 pure-u-md-1-1">
                <div class="input-group">
                    <input type="text" class="pure-input-1" placeholder="Pencarian..." id="keyword">

                    <span class="input-group-btn">
                        <button class="pure-button button-small button-secondary" type="button" id="btn-search">SEARCH</button>
                        <a href="" class="pure-button button-small button-warning">RESET</a>
                    </span>
                </div>
                <div id="view">
                    <?php include "require/modulProduk/view.php"; ?>
                </div>
            </div>
        </div>

    </div>

    <!-- Load File jquery.min.js yang ada difolder js -->
    <script src="assets/js/jquery.min.js"></script>

    <!-- Load File bootstrap.min.js yang ada difolder js -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Load file ajax.js yang ada di folder js -->
    <script src="assets/js/ajax.js"></script>
    <?php include_once "footer.php"; ?>