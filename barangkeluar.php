<?php
require_once "header.php";
require_once "require/barangkeluar/koneksi.php";
error_reporting(E_ALL);
function rupiah($angka){
	
	$hasil_rupiah = "Rp " . number_format($angka,0,',','.');
	return $hasil_rupiah;
 
}

if(!empty($_POST['transaksi'])){
    $pattern = '/([^0-9]+)/'; 
    $id_produk = $_POST['id'];
    $qty = $_POST['qty'];
    $catatan = $_POST['catatan'];
    $id_unit = $_POST['unit_kerja'];

    $queryread = mysqli_query($db,"SELECT * FROM products where id = '$id_produk'");
    $hasilread = mysqli_fetch_array($queryread);
    $qty_last = $hasilread['quantity'];
    $qty_now = $qty_last - $qty;
    $datetime = $_POST['tgltrx'];/* date("Y-m-d H:i:s"); */
    
    $queryInsertA = mysqli_query($db,"INSERT INTO transaksi (id_barang, id_unit, waktu, qty, status, catatan)
     values ('$id_produk','$id_unit','$datetime','$qty','2','$catatan')");
     if($queryInsertA){
        $queryInsertA = mysqli_query($db,"UPDATE products set quantity = '$qty_now' where id = '$id_produk'");
        if(!$queryInsertA){
?>
<script type="text/javascript">
    alert("Gagal! merubah produk..");
    window.location.replace("../../stokbarang.php");
</script>
<?php
        }
     }

}
 ?>
<script type="text/javascript">
    $(document).ready(function () {

        load_data();

        function load_data(query) {
            $.ajax({
                url: "require/barangkeluar/searched.php",
                method: "POST",
                data: {
                    query: query
                },
                success: function (data) {
                    $('#search-result').html(data);
                }
            });
        }

        $('#search_text').keyup(function () {
            var search = $(this).val();
            if (search != '') {
                load_data(search);
            } else {
                load_data();
            }
        });

    });


    function showRow(row) {
        var x = row.cells;
        document.getElementById("id").value = x[0].innerHTML;
        document.getElementById("idpro").value = x[0].innerHTML;
        document.getElementById("nama_barang").value = x[1].innerHTML;
        document.getElementById("kategori").value = x[2].innerHTML;
    }
</script>
<div class="content pure-u-1 pure-u-md-21-24">
    <div class="header-small">

        <div class="items">
            <h1 class="subhead">Input Barang Keluar</h1>
        </div>

        <div class="pure-g">
            <div class="pure-u-7-12">
                <form class="pure-form">
                    <input id="search_text" name="search_text" type="text" class="pure-input-1" placeholder="Cari Barang"
                        style="width:400px;" />
                </form>
                <br>

                <div style="padding-right:15px;" id="search-result"></div>

            </div>
            <div id="purchase" class="pure-u-1-3">
                <form method="post" action="" class="pure-form pure-form-stacked">
                    <fieldset>
                        <legend>Form Transaksi</legend>

                        <input id="idpro" name="id" type="hidden" placeholder="Id barang">

                        <label for="id">Id</label>
                        <input id="id" type="text" style="background-color:#f0ff6b; color:#000;" disabled placeholder="Id barang">

                        <label for="nama_barang">Nama barang</label>
                        <input id="nama_barang" style="background-color:#f0ff6b; color:#000;" type="text" disabled placeholder="Nama barang">

                        <label for="kategori">Kategori</label>
                        <input id="kategori" style="background-color:#f0ff6b; color:#000;" type="text" disabled placeholder="Kategori Barang">

                        <label for="tgltrx">Tgl Transaksi</label>
                        <input required id="tgltrx" name="tgltrx" type="date" placeholder="Tgl Transaksi" value="<?php echo date('Y-m-d'); ?>">

                        <label for="qty">Jumlah Barang</label>
                        <input required id="qty" name="qty" type="text" placeholder="Jumlah Barang">
                        <span class="pure-form-message-inline" id="caution" style="display:none;"></span>

                        <label for="id_label_single">Unit Tujuan</label>
                        <select name="unit_kerja" class="js-example-basic-single js-states form-control" id="unit_kerja" style="width:80%">
                            <option value="0">--Pilih Unit--</option>
                            <?php
                                $query = mysqli_query($db,"SELECT * from unit") or die(mysql_error());
                                while ($hasil = mysqli_fetch_array($query)) {
                                   ?>
                                    <option value="<?php echo $hasil['id_unit'] ?>"><?= $hasil['nama_unit'] ?></option>
                                   <?php
                                }
                            ?>
                            
                        </select>

                        <label for="catatan">Catatan</label>
                        <Textarea id="catatan" name="catatan" placeholder="Catatan"></textarea><br>

                        <input id="button" type="submit" class="pure-button pure-button-primary" name="transaksi" value="Transaksi" />
                    </fieldset>
                </form>
            </div><br>
            <div class="pure-u-1" style="margin-top:40px;">
            <fieldset>
                        <legend>Barang Keluar Terakhir</legend>
                        <table class="pure-table pure-table-striped">
                        <thead>
                            <tr style="background:#429eea;color:#fff;">
                                <th>Tgl Keluar</th>
                                <th>Nama Barang</th>
                                <th>Jumlah</th>
                                <th>Catatan</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>

                        <tbody>
                        <?php
                        $query = mysqli_query($db,"SELECT
                        transaksi.id_transaksi,
                        transaksi.waktu,
                        transaksi.catatan,
                        transaksi.id_barang as idproduk,
                        transaksi.qty as qtyi,
                        transaksi.`status`,
                        transaksi.buy_price as harga,
                        transaksi.sale_price,
                        transaksi.id_unit,
                        products.id,
                        products.`name` as namapro,
                        products.quantity,
                        products.buy_price,
                        products.sale_price,
                        products.satuan,
                        products.categorie_id,
                        products.media_id,
                        products.date,
                        categories.id,
                        categories.`name` as kategori,
                        unit.id_unit,
                        unit.nama_unit
                        FROM
                        transaksi
                        INNER JOIN products ON transaksi.id_barang = products.id
                        INNER JOIN categories ON products.categorie_id = categories.id
                        LEFT OUTER JOIN unit ON transaksi.id_unit = unit.id_unit
                        WHERE
                        transaksi.`status` = 2
                        ORDER BY transaksi.id_transaksi DESC LIMIT 10 OFFSET 0");
                        
                        while($hasilTrx = mysqli_fetch_array($query)){
                         
                        ?>
                                <tr>
                                    <td><?php echo date("d-M-Y", strtotime($hasilTrx['waktu'])); ?></td>
                                    <td><?php echo $hasilTrx['namapro']; ?></td>
                                    <td><?php echo $hasilTrx['qtyi']; echo " ".$hasilTrx['satuan']; ?></td>
                                    <td><?php echo $hasilTrx['nama_unit']." | ".$hasilTrx['catatan']; ?></td>
                                    <td><a class="pure-button pure-button-primary" href="require/barangkeluar/cetakbbk.php?tgl=<?php echo $hasilTrx['waktu']; ?>&catatan=<?php echo $hasilTrx['catatan']; ?>" target="_blank" id="cetak"><i class="fas fa-print"></i></a>
                                    <a class="pure-button button-success" href="require/barangkeluar/prosesbarangkeluar.php?id=<?php echo $hasilTrx['id_transaksi']; ?>&s=edit" id="cetak"><i class="fas fa-edit"></i></a>
                                    <a onclick="return confirm('Yakin akan menghapus?');" class="pure-button button-error" href="require/barangkeluar/prosesbarangkeluar.php?id=<?php echo $hasilTrx['id_transaksi']; ?>&s=del"><i class="fas fa-trash-alt"></i></a></td>
                                </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                        </table>
            </fieldset>
            </div>
        </div>

        <script type="text/javascript">
            var rupiah = document.getElementById('harga_beli');
            rupiah.addEventListener('keyup', function (e) {
                // tambahkan 'Rp.' pada saat form di ketik
                // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
                rupiah.value = formatRupiah(this.value, 'Rp. ');
            });

            //var number = /^[0-9]+$/;
            // var formjumlah = document.getElementById('qty');
            // formjumlah.addEventListener('keyup', function (e) {
            //     if(isNaN(this.value)){
            //         document.getElementById("caution").style.display = "inline";
            //         document.getElementById("caution").style.color = "red";
            //         document.getElementById("caution").innerHTML = "Harus diisi dengan angka";
            //         document.getElementById("button").disabled = true;
            //     }else{
            //         document.getElementById("caution").style.display = "none";
            //         document.getElementById("button").disabled = false;
            //     }
            // });


            /* Fungsi formatRupiah */
            function formatRupiah(angka, prefix) {
                var number_string = angka.replace(/[^,\d]/g, '').toString(),
                    split = number_string.split(','),
                    sisa = split[0].length % 3,
                    rupiah = split[0].substr(0, sisa),
                    ribuan = split[0].substr(sisa).match(/\d{3}/gi);

                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if (ribuan) {
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }

                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
            }
            
        </script>
        <?php require_once "footer.php"; ?>
