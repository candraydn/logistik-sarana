/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 100121
Source Host           : localhost:3306
Source Database       : inventory

Target Server Type    : MYSQL
Target Server Version : 100121
File Encoding         : 65001

Date: 2019-02-18 16:10:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('9', 'Bangunan');
INSERT INTO `categories` VALUES ('10', 'Elektronik');
INSERT INTO `categories` VALUES ('16', 'Gen Set');
INSERT INTO `categories` VALUES ('8', 'Instalasi Air');
INSERT INTO `categories` VALUES ('7', 'Instalasi Listrik');
INSERT INTO `categories` VALUES ('13', 'Log Linen');
INSERT INTO `categories` VALUES ('12', 'Log OB');
INSERT INTO `categories` VALUES ('15', 'Log Sanitasi');
INSERT INTO `categories` VALUES ('14', 'Log Transportasi');

-- ----------------------------
-- Table structure for members
-- ----------------------------
DROP TABLE IF EXISTS `members`;
CREATE TABLE `members` (
  `member_id` int(8) NOT NULL AUTO_INCREMENT,
  `member_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `member_password` varchar(64) NOT NULL,
  `member_email` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of members
-- ----------------------------
INSERT INTO `members` VALUES ('1', 'admin', '$2y$10$pbn3VpzAr0q2cg7lDNMCduDdpzSMM0Pt6wtLao5rhZwPhz9Si0E.i', 'admin@admin.com');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `satuan` varchar(255) DEFAULT NULL,
  `quantity` int(50) DEFAULT '0',
  `buy_price` int(50) DEFAULT '0',
  `sale_price` int(50) NOT NULL,
  `categorie_id` int(11) unsigned NOT NULL,
  `media_id` int(11) DEFAULT '0',
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `categorie_id` (`categorie_id`),
  KEY `media_id` (`media_id`),
  CONSTRAINT `FK_products` FOREIGN KEY (`categorie_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=270 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('1', 'Kabel NYY uk 4x1 1/2 (100m)', 'Roll', '1', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('2', 'Kran Air Besi Onda 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('3', 'Kran Air Besi 3/4\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('4', 'Kran Air Plastik 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('5', 'Kran Air Plastik 3/4\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('6', 'Sock Drat PVC Dalam Biasa 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('7', 'Sock Drat PVC Dalam Biasa 3/4\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('8', 'Sock Drat PVC Dalam Biasa 1\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('9', 'Sock Drat PVC Dalam Biasa 1 1/4\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('10', 'Sock Drat PVC Dalam Biasa 1 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('11', 'Sock Drat PVC Dalam Biasa 2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('12', 'Sock Drat Kuningan Dalam Biasa 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('13', 'Sock Drat Kuningan Dalam Biasa 2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('14', 'Sock Drat PVC Luar 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('15', 'Sock Drat PVC Luar 3/4\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('16', 'Sock Drat PVC Luar 1\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('17', 'Sock Drat PVC Luar 1 1/4\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('18', 'Sock Drat PVC Luar 1 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('19', 'Sock Drat PVC Luar 2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('20', 'Knee PVC Biasa 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('21', 'Knee PVC Biasa 3/4\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('22', 'Knee PVC Biasa 1\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('23', 'Knee PVC Biasa 1 1/4\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('24', 'Knee PVC Biasa 1 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('25', 'Knee PVC Biasa 2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('26', 'Knee PVC Biasa 2 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('27', 'Knee PVC Biasa  3\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('29', 'Knee PVC Biasa 4\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('30', 'Knee PVC Biasa 6\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('31', 'Knee PVC Biasa 8\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('32', 'Knee PVC Drat Dalam Biasa 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('33', 'Knee PVC Drat Dalam Biasa 3/4\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('34', 'Knee Kuningan Drat Dalam Biasa 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('37', 'Knee Kuningan Drat Dalam Biasa 3/4\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('38', 'Tee PVC 1/2 \"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('39', 'Tee PVC 3/4\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('40', 'Tee PVC 1\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('41', 'Tee PVC 1 1/4\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('42', 'Tee PVC 1 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('43', 'Tee PVC 2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('44', 'Tee PVC 2 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('45', 'Tee PVC 3\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('46', 'Tee PVC 4\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('47', 'Tee PVC 6\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('48', 'Tee PVC 8\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('49', 'Sock PVC Biasa 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('50', 'Sock PVC Biasa 3/4\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('51', 'Sock PVC Biasa 1\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('52', 'Sock PVC Biasa 1 1/4\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('53', 'Sock PVC Biasa 1 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('54', 'Sock PVC Biasa 2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('55', 'Sock PVC Biasa 2 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('56', 'Sock PVC Biasa 3\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('57', 'Sock PVC Biasa 4\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('58', 'Sock PVC Biasa 6\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('59', 'Sock PVC Biasa 8\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('60', 'Tee PVC Drat Dalam 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('61', 'Tee PVC Drat Dalam 3/4\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('62', 'Tee Kuningan Drat Dalam 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('63', 'Tee Kuningan Drat Dalam 3/4\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('64', 'Over Sock PVC 1/2x3/4', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('65', 'Over Sock PVC 1/2x1', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('66', 'Over Sock PVC 1/2x1 1/4', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('67', 'Over Sock PVC 3/4x1', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('68', 'Over Tee PVC 1/2x3/4', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('69', 'Over Tee PVC 1/2x1', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('70', 'Over Tee PVC 3/4x1', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('71', 'Kran Shower Besi 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('72', 'Kran Shower T Besi 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('73', 'Shower Toilet 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('74', 'Shower Mandi 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('75', 'Kran Wastafel 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('76', 'Kran Cuci Piring 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('77', 'Kran Air Panas Dingin Besi 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('78', 'Flexibel Air Panas 40Cm', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('79', 'Flexibel Air Panas 50Cm', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('80', 'Flexibel Air Biasa 30Cm', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('81', 'Flexibel Air Biasa 40Cm', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('82', 'Flexibel Air Biasa 50Cm', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('83', 'Sifon Wastafel', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('84', 'Stop Kran PVC 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('85', 'Stop Kran PVC 3/4\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('86', 'Stop Kran PVC 1\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('87', 'Stop Kran PVC 1 1/4\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('88', 'Stop Kran PVC 1 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('89', 'Stop Kran PVC 2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('90', 'Stop Kran Kuningan 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('91', 'Stop Kran Kuningan 3/4\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('92', 'Stop Kran Kuningan 1\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('93', 'Radar(Otomatis Toren)', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('94', 'Kran Pelampung Toren', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('95', 'Paralon PVC 1/2\"', 'Btng', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('96', 'Paralon PVC 3/4\"', 'Btng', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('97', 'Paralon PVC 1\"', 'Btng', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('98', 'Paralon PVC 1 1/4\"', 'Btng', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('99', 'Paralon PVC 1 1/2\"', 'Btng', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('100', 'Paralon PVC 2\"', 'Btng', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('101', 'Paralon PVC 2 1/2\"', 'Btng', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('102', 'Paralon PVC 3\"', 'Btng', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('103', 'Paralon PVC 4\"', 'Btng', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('104', 'Paralon PVC 6\"', 'Btng', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('105', 'Paralon PVC 8\"', 'Btng', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('106', 'Pipa Besi 1/2\"', 'Btng', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('107', 'Pipa Besi 3/4\"', 'Btng', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('108', 'Pipa Besi 1\"', 'Btng', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('109', 'Lampu LED Philips 4 Watt', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('110', 'Lampu LED Philips 6 Watt', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('111', 'Lampu LED Philips 14 1/2 Watt', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('112', 'Lampu Sorot LED 10 Watt', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('113', 'Lampu Sorot LED 30 Watt', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('114', 'Lampu Sorot LED 50 Watt', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('115', 'Kabel Ties 10Cm', 'Pack', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('116', 'Kabel Ties 15Cm', 'Pack', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('117', 'Kabel Ties 20Cm', 'Pack', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('119', 'Kabel Ties 25Cm', 'Pack', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('120', 'Kabel Ties 30Cm', 'Pack', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('121', 'Kabel Ties 40Cm', 'Pack', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('122', 'Fiting Lampu Tempel', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('123', 'Fiting Lampu Gantung', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('124', 'Saklar Tunggal', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('125', 'Saklar Tunggal Tempel', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('126', 'Saklar Double', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('127', 'Saklar Double Tanem', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('128', 'Stop Kontak Tempel Lubang 1', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('129', 'Stop Kontak Tempel Lubang 2', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('130', 'Stop Kontak Tempel Lubang 3', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('131', 'Stop Kontak Tempel Lubang 4', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('132', 'Stop Kontak Tanem', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('133', 'Isolasi Listrik', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('134', 'Klem Kabel No 7', 'Pack', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('135', 'Klem Kabel No 9', 'Pack', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('136', 'Klem Kabel No 10', 'Pack', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('137', 'Klem Kabel No 12', 'Pack', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('139', 'Klem Kabel No 17', 'Pack', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('140', 'Jack Bulat', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('141', 'Jack Gepeng', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('142', 'Kabel NYO 2x075', 'Roll', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('143', 'Kabel NYO 3x075', 'Roll', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('144', 'Kabel NYO 2x1 1/2', 'Roll', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('145', 'Kabel NYO 3x 1 1/2', 'Roll ', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('146', 'Kabel NYM 2x 1 1/2', 'Roll', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('147', 'Kabel NYM 2x 2 1/2', 'Roll', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('148', 'Kabel NYM 3x 1 1/2', 'Roll', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('149', 'Kabel NYM 3x 2 1/2', 'Roll', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('150', 'Kabel NYM 3x4', 'Roll', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('151', 'Kabel NYM 3x6', 'Roll', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('152', 'Kabel NYM 4x 1 1/2', 'Roll', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('153', 'Kabel NYM 4x 2 1/2', 'Roll', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('154', 'Kabel NYM 4x4', 'Roll', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('155', 'Kabel NYM 4x6', 'Roll', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('156', 'Kabel NYM 4x10', 'Roll', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('157', 'Kabel NYM 4x16', 'M', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('158', 'Kabel NYM 4x25', 'M', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('160', 'Kabel NYM 4x35', 'M', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('161', 'Kabel NYM 4x50', 'M', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('162', 'Kreon R.22', 'Tbng', '0', '0', '0', '10', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('163', 'Kreon R 32', 'Tbng', '0', '0', '0', '10', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('164', 'Kreon R 410', 'Tbng', '0', '0', '0', '10', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('165', 'Over Load 1PK', 'Pc', '0', '0', '0', '10', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('166', 'Over Load 2PK', 'Pc', '0', '0', '0', '10', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('167', 'Lakban AC ', 'Pc', '0', '0', '0', '10', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('168', 'Pipa AC 1PK', 'M', '0', '0', '0', '10', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('169', 'Pipa AC 2PK', 'M', '0', '0', '0', '10', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('170', 'Seal Tape', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('171', 'MCB 10 Amp', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('172', 'MCB 16 Amp', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('173', 'MCB 20 Amp     ', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('174', 'MCB 32 Amp', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('175', 'Kapasitor AC 1/2 Mikro', 'Pc', '0', '0', '0', '10', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('176', 'Kapasitor AC 30 Mikro', 'Pc', '0', '0', '0', '10', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('177', 'Sikring Stop Kontak AC 20 Amp', 'Pc', '0', '0', '0', '10', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('178', 'Kuas 1\"', 'Pc', '0', '0', '0', '9', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('179', 'Kuas 2\"', 'Pc', '0', '0', '0', '9', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('180', 'Kuas 3\"', 'Pc', '0', '0', '0', '9', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('181', 'Kuas 4\"', 'Pc', '0', '0', '0', '9', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('182', 'Kuas Roll Kecil', 'Pc', '0', '0', '0', '9', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('183', 'Kuas Roll Besar', 'Pc', '0', '0', '0', '9', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('184', 'Busa Refil Roll Kecil', 'Pc', '0', '0', '0', '9', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('185', 'Busa Refil Roll Besar', 'Pc', '0', '0', '0', '9', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('186', 'Remote AC Panasonic', 'Pc', '0', '0', '0', '10', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('187', 'Remote AC LG', 'Pc', '0', '0', '0', '10', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('188', 'Remote AC Samsung', 'Pc', '0', '0', '0', '10', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('189', 'Remote AC Daikin', 'Pc', '0', '0', '0', '10', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('190', 'Remote TV Panasonic', 'Pc', '0', '0', '0', '10', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('192', 'Remote TV LG', 'Pc', '0', '0', '0', '10', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('193', 'Remote TV Coocaa', 'Pc', '0', '0', '0', '10', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('194', 'Remote TV Samsung', 'Pc', '0', '0', '0', '10', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('195', 'WD 40', 'Klng', '0', '0', '0', '10', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('196', 'Paku Biasa 1 1/2Cm', 'Kg', '0', '0', '0', '9', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('197', 'Paku Biasa 3Cm', 'Kg', '0', '0', '0', '9', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('198', 'Paku Biasa 5Cm', 'Kg', '0', '0', '0', '9', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('199', 'Paku Biasa 7Cm', 'Kg', '0', '0', '0', '9', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('200', 'Paku Biasa 10Cm', 'Kg', '0', '0', '0', '9', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('201', 'Otomatis Pompa Air', 'Pc', '1', '0', '0', '10', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('202', 'Kapasitor Pompa Air 5Um', 'Pc', '0', '0', '0', '10', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('203', 'Kapasitor Pompa Air 8Um', 'Pc', '0', '0', '0', '10', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('204', 'Kapasitor Pompa Air 10Um', 'Pc', '0', '0', '0', '10', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('205', 'Paku Kasibolt', 'Kg', '0', '0', '0', '9', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('206', 'Paku Gysun', 'Kg', '0', '0', '0', '9', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('207', 'Sekrup Gypsun 3Cm', 'Kg', '0', '0', '0', '9', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('208', 'Sekrup Gypsun 5Cm', 'Kg', '0', '0', '0', '9', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('209', 'Sekrup Gypsun 7Cm', 'Kg', '0', '0', '0', '9', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('210', 'Paku Beton 3Cm', 'Kg', '0', '0', '0', '9', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('211', 'Paku Beton 5Cm', 'Kg', '0', '0', '0', '9', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('212', 'Paku Beton 10Cm', 'Kg', '0', '0', '0', '9', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('213', 'Paku Beton 7Cm', 'Kg', '0', '0', '0', '9', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('214', 'Clem Paralon 1/2', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('215', 'Clem Paralon 3/4', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('216', 'Clem Paralon 1', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('219', 'Kabel Telephone Isi 4(90M)', 'Roll', '0', '0', '0', '10', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('220', 'Clem Paralon 1 1/4\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('221', 'Clem Paralon 1 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('222', 'Clem Paralon 2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('223', 'Clem Paralon 2 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('224', 'Clem Paralon 3\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('225', 'Clem Paralon 4\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('226', 'Clem Paralon 6\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('227', 'Clem Paralon 8\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('228', 'Oil Meditran S.40', 'Ltr', '0', '0', '0', '16', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('229', 'Oil Filter', 'Pc', '0', '0', '0', '16', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('230', 'Fuel Filter', 'Pc', '0', '0', '0', '16', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('231', 'Filter Udara', 'Pc', '0', '0', '0', '16', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('232', 'BBM Solar', 'Ltr', '0', '0', '0', '16', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('233', 'BBM Bensin', 'Ltr', '0', '0', '0', '15', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('234', 'Soklin Pembesih Lantai Refil 800 ml', 'Pc', '0', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('235', 'Karbol Refil 800 ml', 'Pc', '0', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('236', 'Proclin Pemutih Btl 500 ml', 'Pc', '0', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('237', 'Kantong Plastik Sampah Hitam', 'Pack', '0', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('238', 'Kantong Plastik Sampah Kuning Kecil', 'Pack', '0', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('239', 'Hit Spray Btl 600 ml', 'Pc', '0', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('240', 'WPC Porselen Btl 800 ml', 'Pc', '0', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('241', 'Deterjen BBK 390 Gr', 'Pc', '0', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('242', 'Kresek Hitam Sedang', 'Pack', '0', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('243', 'Kresek Hitam Jumbo', 'Pack', '0', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('244', 'Stella Gantung 42 Gr', 'Pc', '0', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('245', 'Kamper Ball Isi 5', 'Pack', '0', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('246', 'Sabut Spon', 'Pc', '0', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('247', 'Sabut Stainless', 'Pc', '0', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('248', 'Lap Pel Refil', 'Pc', '0', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('249', 'Sapu Air', 'Pc', '0', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('250', 'Sapu Lantai', 'Pc', '1', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('251', 'Automatic Spray Refil 225 ml', 'Pc', '0', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('252', 'Handskun Rumah Tangga', 'Set', '0', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('253', 'Safety Box', 'Pc', '40', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('254', 'Tisue Gulung', 'Pc', '14', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('255', 'Pengharum Spray 450 ml', 'Pc', '0', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('256', 'Hand Soap Refil 5Ltr', 'Gln', '2', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('257', 'Hand Scrap Refil 5Ltr', 'Gln', '4', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('258', 'Lap Pel Refil Lobang 24\"', 'Pc', '0', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('259', 'Kain Flanel', 'Pc', '0', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('260', 'B Baterai Energizer AA', 'Pc', '0', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('261', 'Kanebo', 'Pc', '1', '0', '0', '12', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('262', 'Semen 50Kg', 'Sak', '0', '0', '0', '9', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('263', 'Hose Air PVC 5/8\"', 'Roll', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('264', 'Hose Air PVC 5/8', 'Roll', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('265', 'Lampu LED Philips 9 Watt', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('266', 'Kabel NYY Uk 2x1 1/2(100m)', 'Roll', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('267', 'Lampu Taman Bulat', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('268', 'Kran Air Stainless Onda 1/2\"', 'Pc', '0', '0', '0', '8', '0', '0000-00-00 00:00:00');
INSERT INTO `products` VALUES ('269', 'Lampu TL LED Philips 8Watt', 'Pc', '0', '0', '0', '7', '0', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for transaksi
-- ----------------------------
DROP TABLE IF EXISTS `transaksi`;
CREATE TABLE `transaksi` (
  `id_transaksi` int(10) NOT NULL AUTO_INCREMENT,
  `id_barang` int(10) DEFAULT NULL,
  `waktu` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `supplier` varchar(255) DEFAULT NULL,
  `nofaktur` varchar(255) DEFAULT NULL,
  `tglfaktur` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `qty` int(11) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `buy_price` int(10) DEFAULT NULL,
  `sale_price` int(10) DEFAULT NULL,
  `catatan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_transaksi`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of transaksi
-- ----------------------------
INSERT INTO `transaksi` VALUES ('3', '1', '2019-02-12 15:53:11', 'Tukuiki', '035704', '2019-02-04 00:00:00', '3', '1', '1688000', '1688000', null);
INSERT INTO `transaksi` VALUES ('4', '1', '2019-02-12 15:58:48', null, null, null, '2', '2', null, null, 'U/Kep Instalasi Listrik Penerangan Danau RSOP');
INSERT INTO `transaksi` VALUES ('5', '201', '2019-02-13 14:15:59', 'TK Surya Indah', '0000342', '2019-02-13 00:00:00', '2', '1', '66500', '66500', null);
INSERT INTO `transaksi` VALUES ('6', '201', '2019-02-13 14:32:06', null, null, null, '1', '2', null, null, 'U/Kep Penggantian di Pompa Air RI');
INSERT INTO `transaksi` VALUES ('7', '253', '2019-02-13 14:36:04', 'UD Soka Damai', 'Saldo Awal', '2019-02-13 00:00:00', '40', '1', '0', '0', null);
INSERT INTO `transaksi` VALUES ('8', '261', '2019-02-13 14:41:03', 'UD Soka Damai', 'Saldo Awal', '2019-02-13 00:00:00', '2', '1', '0', '0', null);
INSERT INTO `transaksi` VALUES ('9', '256', '2019-02-13 14:43:48', 'UD Soka Damai', 'Saldo Awal', '2019-02-13 00:00:00', '3', '1', '0', '0', null);
INSERT INTO `transaksi` VALUES ('10', '257', '2019-02-13 14:46:02', 'PT IHS', 'Saldo Awal', '2019-02-13 00:00:00', '4', '1', '0', '0', null);
INSERT INTO `transaksi` VALUES ('11', '254', '2019-02-13 14:48:03', 'UD Soka Damai', 'Saldo Awal', '2019-02-13 00:00:00', '14', '1', '0', '0', null);
INSERT INTO `transaksi` VALUES ('12', '250', '2019-02-13 14:48:52', 'UD Soka Damai', 'Saldo Awal', '2019-02-13 00:00:00', '1', '1', '0', '0', null);
INSERT INTO `transaksi` VALUES ('13', '261', '2019-02-14 10:51:50', null, null, null, '1', '2', null, null, 'U/Kep Ops OB');
INSERT INTO `transaksi` VALUES ('14', '256', '2019-02-14 10:53:19', null, null, null, '1', '2', null, null, 'U/Kep Ops OB');
SET FOREIGN_KEY_CHECKS=1;
