<?php
  session_start();

  require_once "../authCookieSessionValidate.php";

  if(!$isLoggedIn) {
      header("Location: ../../");
  }
?>

<!doctype html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
    integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>  
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A layout example that shows off a blog page with a list of posts.">
    <title>RSOP</title>
    <link rel="stylesheet" href="../../assets/css/pure-min.css">
    <link rel="stylesheet" href="../../assets/css/pure-responsive-min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
</head>
<body>
    <div id="layout" class="pure-g">
        <div class="sidebar pure-u-1 pure-u-md-3-24">
            <div id="menu">
                <div class="pure-menu">
                    <p class="pure-menu-heading">
                        RSOP
                        <a href="require/logout.php" class="pure-button button-xxsmall">OUT &raquo;</a>
                    </p>
                    <ul class="pure-menu-list">
                        <li>
                            <a href="../../dashboard.php" class="pure-menu-link"><i class="fas fa-home"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="../../stokbarang.php" class="pure-menu-link"><i class="fas fa-bars"></i> Stok Barang</a>
                        </li>
                        <li>
                            <a href="../../kategori.php" class="pure-menu-link"><i class="fas fa-bookmark"></i> Kategori</a>
                        </li>
                        <li class="menu-item-divided">
                            <a href="../../barangmasuk.php" class="pure-menu-link"><i class="fas fa-chevron-circle-right"></i> Barang Masuk</a>
                        </li>
                        <li class="menu-item-divided">
                            <a href="../../barangkeluar.php" class="pure-menu-link"><i class="fas fa-chevron-circle-left"></i> Barang Keluar</a>
                        </li>
                        <li>
                            <a href="laporan.php" class="pure-menu-link"><i class="fas fa-book"></i> Laporan</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content pure-u-1 pure-u-md-21-24">
            <div class="header-small">

                <div class="items"> 
                    <h1 class="subhead">Edit Barang</h1>
                </div>
                        <?php
                                include_once "koneksi.php";
                                $id = $_GET['id'];
								$no = 0;
								$sql = "SELECT
										* from products where id = '$id'
										";
								$queryPro = mysqli_query($db, $sql);
                                $produk = mysqli_fetch_array($queryPro);
                                $id_categ = $produk['categorie_id'];
								$id_unit = $produk['unit'];
						?>
                <div class="pure-g">
                    <div class="pure-u-1 pure-u-md-1-1">
                        <div class="column-block" ng-app="liveApp" ng-controller="liveController" ng-init="fetchData()">
						<form action="prosesproduk.php?status=edit" method="post" novalidate autocomplete="off" class="pure-form pure-form-stacked">
							<fieldset>
                            <input name="idpro" type="hidden" value="<?php echo $_GET['id']; ?>">
								<label>Nama Produk</label>
								<input name="nama" type="text" placeholder="Nama" class="pure-input-1" value="<?php echo $produk['name']; ?>">

								<label>Satuan</label>
								<input name="satuan" type="text" placeholder="Satuan Barang" class="pure-input-1" value="<?php echo $produk['satuan']; ?>">

                                <label>Kategori</label>
								<select name="kategori" class="pure-input-1">
								<?php
									$sql = "SELECT
											*
											FROM
											categories where id = '$id_categ'
											";
									$query = mysqli_query($db, $sql);
									$categories = mysqli_fetch_array($query)
									
								?>
									<option selected value="<?php echo $categories['id']; ?>"><?php echo $categories['name']; ?></option>
                                <?php
									$sql = "SELECT
											*
											FROM
											categories where id <> '$id_categ'
											";
									$query = mysqli_query($db, $sql);
									while($categories = mysqli_fetch_array($query)){
								?>
                                    <option value="<?php echo $categories['id']; ?>"><?php echo $categories['name']; ?></option>
                                    <?php
                                        }
				                    ?>
								</select>
								
								<label>Unit Kerja</label>
								<select name="unit" class="pure-input-1">
								<?php
									$sql = "SELECT
											*
											FROM
											unit where id_unit = '$id_unit'
											";
									$query = mysqli_query($db, $sql);
									$unit = mysqli_fetch_array($query)
									
								?>
									<option selected value="<?php echo $unit['id_unit']; ?>"><?php echo $unit['nama_unit']; ?></option>
                                <?php
									$sql = "SELECT
											*
											FROM
											unit where id_unit <> '$id_unit'
											";
									$query = mysqli_query($db, $sql);
									while($unit = mysqli_fetch_array($query)){
								?>
                                    <option value="<?php echo $unit['id_unit']; ?>"><?php echo $unit['nama_unit']; ?></option>
                                    <?php
                                        }
				                    ?>
								</select>

								<input type="hidden" name="id" value="1">
								<button type="submit" class="pure-button button-success">Simpan</button>
							</fieldset>
						</form>
                        </div>
                    </div>
                  </div>


                <?php require_once "../../footer.php"; ?>
