<?php
  session_start();

  require_once "../authCookieSessionValidate.php";

  if(!$isLoggedIn) {
      header("Location: ./");
  }

  function rupiah($angka){
	
	$hasil_rupiah = "Rp " . number_format($angka,0,',','.');
	return $hasil_rupiah;
 
}
?>

<!doctype html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
    integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>  
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A layout example that shows off a blog page with a list of posts.">
    <title>RSOP</title>
    <link rel="stylesheet" href="../../assets/css/pure-min.css">
    <link rel="stylesheet" href="../../assets/css/pure-responsive-min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
</head>
<body>
    <div id="layout" class="pure-g">
        <div class="sidebar pure-u-1 pure-u-md-3-24">
            <div id="menu">
                <div class="pure-menu">
                    <p class="pure-menu-heading">
                        RSOP
                        <a href="require/logout.php" class="pure-button button-xxsmall">OUT &raquo;</a>
                    </p>
                    <ul class="pure-menu-list">
                        <li>
                            <a href="../../dashboard.php" class="pure-menu-link"><i class="fas fa-home"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="../../stokbarang.php" class="pure-menu-link"><i class="fas fa-bars"></i> Stok Barang</a>
                        </li>
                        <li>
                            <a href="../../kategori.php" class="pure-menu-link"><i class="fas fa-bookmark"></i> Kategori</a>
                        </li>
                        <li class="menu-item-divided">
                            <a href="../../barangmasuk.php" class="pure-menu-link"><i class="fas fa-chevron-circle-right"></i> Barang Masuk</a>
                        </li>
                        <li class="menu-item-divided">
                            <a href="../../barangkeluar.php" class="pure-menu-link"><i class="fas fa-chevron-circle-left"></i> Barang Keluar</a>
                        </li>
                        <li>
                            <a href="../../laporan.php" class="pure-menu-link"><i class="fas fa-book"></i> Laporan</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content pure-u-1 pure-u-md-21-24">
            <div class="header-small">

                <div class="items"> 
                    <h1 class="subhead">Arus Barang</h1>
                </div>

                <div class="pure-g">
                    <div class="pure-u-1 pure-u-md-1-1">
                        <div class="column-block" ng-app="liveApp" ng-controller="liveController" ng-init="fetchData()">
                        <?php
                            include_once "koneksi.php";
                            $id = $_GET['id'];
                            $sql = "SELECT
                            transaksi.id_transaksi,
                            transaksi.waktu,
                            transaksi.id_barang as idproduk,
                            transaksi.qty as qtyi,
                            transaksi.`status`,
                            transaksi.buy_price as harga,
                            transaksi.sale_price,
                            transaksi.tglfaktur,
                            transaksi.nofaktur,
                            transaksi.supplier,
                            transaksi.catatan,
                            products.id,
                            products.`name` as namapro,
                            products.quantity,
                            products.buy_price,
                            products.sale_price,
                            products.categorie_id,
                            products.satuan,
                            products.media_id,
                            products.date,
                            categories.id,
                            categories.`name` as kategori
                            FROM
                            transaksi
                            INNER JOIN products ON transaksi.id_barang = products.id
                            INNER JOIN categories ON products.categorie_id = categories.id
                            WHERE
                            transaksi.id_barang = '$id'
                            ORDER BY transaksi.id_transaksi DESC";
                            $queryPro = mysqli_query($db, $sql);
                            $produk = mysqli_fetch_array($queryPro);
                            $totali = mysqli_num_rows($queryPro);
                            $id_categ = $produk['categorie_id'];

                            ?>
						<h4><?= $produk['namapro'] ?></h4>
                        <table class="pure-table pure-table-bordered">
                            <thead>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>No Faktur / Keperluan</th>
                                    <th>Qty | Stok</th>
                                    <th>Harga Beli</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                $total = 0;
                                $queryPro2 = mysqli_query($db, $sql);
                                while($produk2 = mysqli_fetch_array($queryPro2)){
                                    if($produk2['status']==1){
                                        $total += $produk2['qtyi'];
                                    }else{
                                        $total -= $produk2['qtyi'];
                                    }
                                }
                                $totalseluruh = $total;
                                $queryPro1 = mysqli_query($db, $sql);
                                while($produk = mysqli_fetch_array($queryPro1)){
                                if($produk['status']==1){
                                    
                            ?>
                                <tr style="background-color:#fdffa8;">
                                    <td><?= date("d-M-Y", strtotime($produk['tglfaktur'])); ?></td>
                                    <td><?= $produk['nofaktur']." | ".$produk['supplier']; ?></td>
                                    <td><?= $produk['qtyi']." | ".$total; ?></td>
                                    <td><?= rupiah($produk['harga']); ?></td>
                                    <td><button class="button-warning pure-button">Barang Masuk</button></td>
                                </tr>
                            <?php
                            $total -= $produk['qtyi'];
                                }else{
                                    
                            ?>
                                <tr style="background-color:#ffd7a8;">
                                    <td><?= date("d-M-Y", strtotime($produk['waktu'])); ?></td>
                                    <td><?= $produk['catatan']; ?></td>
                                    <td><?= $produk['qtyi']." | ".$total; ?> </td>
                                    <td>-</td>
                                    <td><button class="button-error pure-button">Barang Keluar</button></td>
                                </tr>
                            <?php
                            $total += $produk['qtyi'];
                                }
                            }
                            $query= mysqli_query($db,"UPDATE products set quantity = '$totalseluruh' where id='$id'") or die(mysql_error());
                            if($query){

                            }else{
                                echo "error";
                            }
                            ?>
                            </tbody>
                        </table>
                        </div>
                    </div>
                  </div>


                <?php require_once "../../footer.php"; ?>
