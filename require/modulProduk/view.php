<br>
<div>
	<table class="pure-table pure-table-bordered">
		<thead>
			<tr>
				<th>#</th>
				<th>Nama Barang Barang</th>
				<th>Kategori</th>
				<th>Unit</th>
				<th>Stok</th>
				<th>Harga Beli</th>
				<th width="20%">Option</th>
			</tr>
		</thead>
		<?php
		// Include / load file koneksi.php
		include "koneksi.php";
		
		// Cek apakah terdapat data page pada URL
		$page = (isset($_POST['page']))? $_POST['page'] : 1;

		$limit = 20; // Jumlah data per halamannya

		// Untuk menentukan dari data ke berapa yang akan ditampilkan pada tabel yang ada di database
		$limit_start = ($page - 1) * $limit;
		
		// Cek apakah variabel data search tersedia
		// Artinya cek apakah user telah mengklik tombol search atau belum
		if(isset($_POST['search']) && $_POST['search'] == true){ // Jika ada data search yg dikirim (user telah mengklik tombol search) dan search sama dengan true
			// variabel $keyword ini berasal dari file search.php,
			// dimana isinya adalah apa yang diinput oleh user pada textbox pencarian
			$param = '%'.mysqli_real_escape_string($db, $keyword).'%';
			
			// Buat query untuk menampilkan data siswa berdasarkan NIS / Nama / Jenis Kelamin / Telp / Alamat
			// dan sesuai limit yang ditentukan
			$sql = mysqli_query($db, "SELECT products.id as idproduk,
			products.`name`,
			products.satuan,
			products.quantity,
			products.categorie_id,
			categories.id,
			categories.`name` AS kategori,
			products.buy_price,
			unit.nama_unit,
			products.sale_price FROM products 
			INNER JOIN unit ON products.unit = unit.id_unit
			INNER JOIN categories ON products.categorie_id = categories.id WHERE products.`name` LIKE '".$param."' LIMIT ".$limit_start.",".$limit);
			
			// Buat query untuk menghitung semua jumlah data
			// dengan keyword yang telah di input
			$sql2 = mysqli_query($db, "SELECT COUNT(*) AS jumlah FROM products INNER JOIN categories ON products.categorie_id = categories.id WHERE products.`name` LIKE '%".$param."%'");
			$get_jumlah = mysqli_fetch_array($sql2);
		}else{ // Jika user belum mengklik tombol search (PROSES TANPA AJAX)
			// Buat query untuk menampilkan semua data siswa
			$sql = mysqli_query($db, "SELECT
			products.id as idproduk,
			products.`name`,
			products.satuan,
			products.quantity,
			products.categorie_id,
			categories.id,
			categories.`name` AS kategori,
			products.buy_price,
			products.sale_price,
			unit.nama_unit,
			unit.id_unit
			FROM
			products
			INNER JOIN unit ON products.unit = unit.id_unit
			INNER JOIN categories ON products.categorie_id = categories.id order by unit.id_unit ASC LIMIT ".$limit_start.",".$limit);
			
			// Buat query untuk menghitung semua jumlah data
			$sql2 = mysqli_query($db, "SELECT COUNT(*) AS jumlah FROM products INNER JOIN unit ON products.unit = unit.id_unit INNER JOIN categories ON products.categorie_id = categories.id");
			$get_jumlah = mysqli_fetch_array($sql2);
		}
		$no = $limit_start;
		while($data = mysqli_fetch_array($sql)){ 
			$no++;// Ambil semua data dari hasil eksekusi $sql
		?>
		<tr>
			<td class="align-middle">
				<?php echo $no; ?>
			</td>
			<td class="align-middle">
				<?php echo $data['name']; ?>
			</td>
			<td class="align-middle">
				<?php echo $data['kategori']; ?>
			</td>
			<td class="align-middle">
				<?php echo $data['nama_unit']; ?>
			</td>
			<td class="align-middle">
				<?php echo $data['quantity']; echo " ".$data['satuan']; ?>
			</td>
			<td class="align-middle">
				<?php echo rupiah($data['buy_price']); ?>
			</td>
			<td class="align-middle">
				<a class="pure-button button-small button-success" href="require/modulProduk/editProduk.php?id=<?php echo $data['idproduk']; ?>"><i class="fas fa-edit"></i></a>
				<a onclick="return confirm('Yakin mau menghapus produk?')" class="pure-button button-small button-warning" href="require/modulProduk/prosesproduk.php?id=<?php echo $data['idproduk']; ?>&status=delete"><i class="fas fa-trash-alt"></i></a>
				<a class="pure-button button-small button-success" href="require/modulProduk/arusProduk.php?id=<?php echo $data['idproduk']; ?>"><i class="fas fa-angle-double-right"></i></a>
			</td>
		</tr>
		<?php
		}
		?>
	</table>
</div>
<br><br>
<!--
-- Buat Paginationnya
-- Dengan bootstrap, kita jadi dimudahkan untuk membuat tombol-tombol pagination dengan design yang bagus tentunya
-->

	<!-- LINK FIRST AND PREV -->
	<?php
	if($page == 1){ // Jika page adalah page ke 1, maka disable link PREV
	?>
	<!-- <li class="disabled"><a href="#">First</a></li>
	<li class="disabled"><a href="#">&laquo;</a></li> !-->
	<?php
	}else{ // Jika page bukan page ke 1
		$link_prev = ($page > 1)? $page - 1 : 1;
	?>
	<a class="pure-button" href="javascript:void(0);" onclick="searchWithPagination(1, false)">First</a>
	<a class="pure-button" href="javascript:void(0);" onclick="searchWithPagination(<?php echo $link_prev; ?>, false)">&laquo;</a>
	<?php
	}
	?>

	<!-- LINK NUMBER -->
	<?php
	$jumlah_page = ceil($get_jumlah['jumlah'] / $limit); // Hitung jumlah halamannya
	$jumlah_number = 3; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
	$start_number = ($page > $jumlah_number)? $page - $jumlah_number : 1; // Untuk awal link number
	$end_number = ($page < ($jumlah_page - $jumlah_number))? $page + $jumlah_number : $jumlah_page; // Untuk akhir link number
	
	for($i = $start_number; $i <= $end_number; $i++){
		$link_active = ($page == $i)? ' pure-button-disabled ' : '';
	?>
	<a class="<?php echo $link_active; ?>pure-button" href="javascript:void(0);" onclick="searchWithPagination(<?php echo $i; ?>, false)">
			<?php echo $i; ?></a>
		<?php
	}
	?>

		<!-- LINK NEXT AND LAST -->
		<?php
	// Jika page sama dengan jumlah page, maka disable link NEXT nya
	// Artinya page tersebut adalah page terakhir 
	if($page == $jumlah_page){ // Jika page terakhir
	?>
		
		<?php
	}else{ // Jika Bukan page terakhir
		$link_next = ($page < $jumlah_page)? $page + 1 : $jumlah_page;
	?>
		<a class="pure-button" href="javascript:void(0);" onclick="searchWithPagination(<?php echo $link_next; ?>, false)">&raquo;</a>
		<a class="pure-button" href="javascript:void(0);" onclick="searchWithPagination(<?php echo $jumlah_page; ?>, false)">Last</a>
		<?php
	}
	?>

