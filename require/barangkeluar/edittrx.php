<?php
  session_start();

  require_once "../authCookieSessionValidate.php";

  if(!$isLoggedIn) {
      header("Location: ./");
  }
?>

<!doctype html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
    integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>  
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A layout example that shows off a blog page with a list of posts.">
    <title>RSOP</title>
    <link rel="stylesheet" href="../../assets/css/pure-min.css">
    <link rel="stylesheet" href="../../assets/css/pure-responsive-min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
</head>
<body>
    <div id="layout" class="pure-g">
        <div class="sidebar pure-u-1 pure-u-md-3-24">
            <div id="menu">
                <div class="pure-menu">
                    <p class="pure-menu-heading">
                        RSOP
                        <a href="require/logout.php" class="pure-button button-xxsmall">OUT &raquo;</a>
                    </p>
                    <ul class="pure-menu-list">
                        <li>
                            <a href="../../dashboard.php" class="pure-menu-link"><i class="fas fa-home"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="../../stokbarang.php" class="pure-menu-link"><i class="fas fa-bars"></i> Stok Barang</a>
                        </li>
                        <li>
                            <a href="../../kategori.php" class="pure-menu-link"><i class="fas fa-bookmark"></i> Kategori</a>
                        </li>
                        <li class="menu-item-divided">
                            <a href="../../barangmasuk.php" class="pure-menu-link"><i class="fas fa-chevron-circle-right"></i> Barang Masuk</a>
                        </li>
                        <li class="menu-item-divided">
                            <a href="../../barangkeluar.php" class="pure-menu-link"><i class="fas fa-chevron-circle-left"></i> Barang Keluar</a>
                        </li>
                        <li>
                            <a href="laporan.php" class="pure-menu-link"><i class="fas fa-book"></i> Laporan</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content pure-u-1 pure-u-md-21-24">
            <div class="header-small">

                <div class="items"> 
                    <h1 class="subhead">Edit Transaksi</h1>
                </div>

                <div class="pure-g">
                    <div class="pure-u-1 pure-u-md-1-1">
                        <div class="column-block">
						<form action="" method="post" class="pure-form pure-form-stacked">
                        <?php
                            $query = mysqli_query($db,"SELECT
                            transaksi.id_transaksi,
                            transaksi.waktu,
                            transaksi.catatan,
                            transaksi.id_barang as idproduk,
                            transaksi.qty as qtyi,
                            transaksi.`status`,
                            transaksi.buy_price as harga,
                            transaksi.sale_price,
                            transaksi.id_unit,
                            products.id as idpro,
                            products.`name` as namapro,
                            products.quantity,
                            products.buy_price,
                            products.sale_price,
                            products.satuan,
                            products.categorie_id,
                            products.media_id,
                            products.date,
                            categories.id,
                            categories.`name` as kategori,
                            unit.id_unit,
                            unit.nama_unit
                            FROM
                            transaksi
                            INNER JOIN products ON transaksi.id_barang = products.id
                            INNER JOIN categories ON products.categorie_id = categories.id
                            LEFT OUTER JOIN unit ON transaksi.id_unit = unit.id_unit
                            WHERE
                            transaksi.id_transaksi = '$idtrx'");
                            
                            $hasilTrx = mysqli_fetch_array($query);

                        ?>
                            <label for="tgltrx">Tgl Transaksi</label>
                            <input required id="tgltrx" name="tgltrx" type="date" placeholder="Tgl Transaksi" value="<?php echo date("Y-m-d", strtotime($hasilTrx['waktu'])); ?>">

                            <label for="nama_barang">Nama barang</label>
                            <input id="nama_barang" type="text" disabled value="<?php echo $hasilTrx['namapro'] ?>">

                            <label for="qty">Jumlah Barang</label>
                            <input required id="qty" name="idbarang" type="hidden" value="<?php echo $hasilTrx['idpro'] ?>">
                            <input required id="qty" name="qtylama" type="hidden" value="<?php echo $hasilTrx['qtyi'] ?>">
                            <input required id="qty" name="qty" type="text" value="<?php echo $hasilTrx['qtyi'] ?>">
                            <span class="pure-form-message-inline" id="caution" style="display:none;"></span>

                            <label for="id_label_single">Unit Tujuan</label>
                            <select name="unit_kerja" class="js-example-basic-single js-states form-control" id="unit_kerja" style="width:30%">
                                <option value="<?php echo $hasilTrx['id_unit'] ?>"><?= $hasilTrx['nama_unit'] ?></option>
                                <?php
                                    $cek_id_unit = $hasilTrx['id_unit'];
                                    $query = mysqli_query($db,"SELECT * from unit where id_unit NOT LIKE '$cek_id_unit'") or die(mysql_error());
                                    while ($hasil1 = mysqli_fetch_array($query)) {
                                    ?>
                                        <option value="<?php echo $hasil1['id_unit'] ?>"><?= $hasil1['nama_unit'] ?></option>
                                    <?php
                                    }
                                ?>
                                
                            </select>

                            <label for="catatan">Catatan</label>
                            <Textarea style="height:100px; width:300px;" id="catatan" name="catatan"><?php echo $hasilTrx['catatan'] ?></textarea><br>

                            <input id="button" type="submit" class="pure-button button-success" name="transaksi" value="Edit" />
                            <a href="../../barangkeluar.php" class="pure-button button-error">Cancel</a>
							
						</form>
                        </div>
                    </div>
                  </div>
            <script type="text/javascript">

            var number = /^[0-9]+$/;
            var formjumlah = document.getElementById('qty');
            formjumlah.addEventListener('keyup', function (e) {
                if(!this.value.match(number)){
                    document.getElementById("caution").style.display = "inline";
                    document.getElementById("caution").style.color = "red";
                    document.getElementById("caution").innerHTML = "Harus diisi dengan angka";
                    document.getElementById("button").disabled = true;
                }else{
                    document.getElementById("caution").style.display = "none";
                    document.getElementById("button").disabled = false;
                }
            });
            </script>

                <?php require_once "../../footer.php"; ?>
