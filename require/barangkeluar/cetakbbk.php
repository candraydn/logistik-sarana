<!DOCTYPE html>
<html>

<head>
    <title>Cetak BBK wew</title>
</head>

<body>
<style>
body{
    width:100%;
}
</style>
<?php 
    include_once "koneksi.php";
    $tgl = date("Y-m-d", strtotime($_GET['tgl']));
    $catatan = $_GET['catatan'];
    
    $query1 = mysqli_query($db,"SELECT
                transaksi.id_transaksi,
                transaksi.id_barang,
                transaksi.waktu,
                transaksi.tglfaktur,
                transaksi.`status`,
                transaksi.catatan,
                transaksi.qty,
                products.id,
                products.`name` as namapro,
                products.satuan
                FROM
                transaksi
                INNER JOIN products ON transaksi.id_barang = products.id
                where transaksi.waktu like '%".$tgl."%' and
                transaksi.catatan like '%".$catatan."%'
                ") or die(mysql_error());
                $hasiltrx = mysqli_fetch_array($query1);
                $tgl2 = date("d M Y", strtotime($hasiltrx['waktu']));
    
?>
<button onclick="printContent('printarea')">Cetak Faktur</button>
<div id="printarea" style="width:100%; padding-left:20px">
<div style="position:absolute; width:100%">
    <img src="../../assets/logo.jpg" width="50px" style="margin-left: 5px;">
</div>
<div style="width:100%; padding-left:70px   ">
    <h5>RS. Orthopaedi Purwokerto
        <br />Jl. Soepardjo Rustam No.99 Purwokerto
        <br />Telp. 0281-6844166/6844199|Email rsop4all@rsop.co.id</h5>
</div>
<hr>
    <div style="text-align:center;width:350px;margin:1px auto; border:solid 1px #000;">
        <h3>Bukti Barang Keluar</h3>
    </div>
    <div style="width:90%;margin:1px auto;">
        <br />
        <table align="center" width="50%" border=2px cellspacing=0px cellpadding="5px">

            <tr>
                <th width="7%">No</th>
                <th>Nama Barang</th>
                <th>Jumlah</th>
                <th>Keterangan</th>
            </tr>
            <?php
                $no = 0;
                $query = mysqli_query($db,"SELECT
                transaksi.id_transaksi,
                transaksi.id_barang,
                transaksi.waktu,
                transaksi.tglfaktur,
                transaksi.`status`,
                transaksi.catatan,
                transaksi.qty,
                products.id,
                products.`name` as namapro,
                products.satuan
                FROM
                transaksi
                INNER JOIN products ON transaksi.id_barang = products.id
                where transaksi.waktu like '%".$tgl."%' and
                transaksi.catatan like '%".$catatan."%'
                ") or die(mysql_error());
                while($hasiltrx1 = mysqli_fetch_array($query)){
                $no++
            ?>
            <tr>
                <td><?php echo $no ?></td>
                <td><?php echo $hasiltrx1['namapro'] ?></td>
                <td><?php echo $hasiltrx1['qty']; echo " ".$hasiltrx1['satuan']; ?></td>
                <td><?php echo $hasiltrx['catatan']; ?></td>
            </tr>
            <?php
                }
            ?>
        </table><br />
        <p align="right">Purwokerto, <?php echo $tgl2; ?></p>
        <div style="float:left">
            <br /><br /><br />
            <hr style="border: 1px solid #000">
            <b>(Penerima)</b>
        </div>
        <div style="float:right">
            <br /><br /><br />
            <hr style="border: 1px solid #000">
            <b>(Adm. Gudang)</b>
        </div>
    </div>
</div>
    <script>
    function printContent(el){
        var restorepage = document.body.innerHTML;
        var printcontent = document.getElementById(el).innerHTML;
        document.body.innerHTML = printcontent;
        window.print();
        document.body.innerHTML = restorepage;
    }
    </script>
</body>

</html>