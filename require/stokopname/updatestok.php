<?php
  session_start();

  require_once "../authCookieSessionValidate.php";

  if(!$isLoggedIn) {
      header("Location: ../../");
  }
?>

<!doctype html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
    integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A layout example that shows off a blog page with a list of posts.">
    <title>RSOP</title>
    <link rel="stylesheet" href="../../assets/css/pure-min.css">
    <link rel="stylesheet" href="../../assets/css/pure-responsive-min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
</head>
<body>
    <div id="layout" class="pure-g">
        <div class="sidebar pure-u-1 pure-u-md-3-24">
            <div id="menu">
                <div class="pure-menu">
                    <p class="pure-menu-heading">
                        RSOP
                        <a href="require/logout.php" class="pure-button button-xxsmall">OUT &raquo;</a>
                    </p>
                    <ul class="pure-menu-list">
                        <li>
                            <a href="../../dashboard.php" class="pure-menu-link"><i class="fas fa-home"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="../../stokbarang.php" class="pure-menu-link"><i class="fas fa-bars"></i> Stok Barang</a>
                        </li>
                        <li>
                            <a href="../../kategori.php" class="pure-menu-link"><i class="fas fa-bookmark"></i> Kategori</a>
                        </li>
                        <li class="menu-item-divided">
                            <a href="../../barangmasuk.php" class="pure-menu-link"><i class="fas fa-chevron-circle-right"></i> Barang Masuk</a>
                        </li>
                        <li class="menu-item-divided">
                            <a href="../../barangkeluar.php" class="pure-menu-link"><i class="fas fa-chevron-circle-left"></i> Barang Keluar</a>
                        </li>
                        <li>
                            <a href="laporan.php" class="pure-menu-link"><i class="fas fa-book"></i> Laporan</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content pure-u-1 pure-u-md-21-24">
            <div class="header-small">

                <div class="items"> 
                    <h1 class="subhead">Edit Stok</h1>
                </div>
                        <?php
                                $id = $_GET['id'];
                                $no = 0;
                                
								$con = new pdo('mysql:host=localhost;dbname=inventory', 'root', '');

                                $query = $con->prepare("SELECT * FROM products where id = '$id'");
                                $query->execute();
                                $produk = $query->fetch();

                                if(isset($_POST['update'])){
                                    $id = $_POST['id'];
                                    $upstok = $_POST['update'];
                                    $stok = $_POST['now'];

                                    if($upstok > $stok){
                                        $stok = $upstok-$stok;
                                        $upPro = $con->prepare("UPDATE products set quantity = '$upstok' where id = '$id'");
                                        $upPro->execute();

                                        $datetime = date("Y-m-d H:i:s");
                                        $crePro = $con->prepare("INSERT INTO transaksi (id_barang, waktu, qty, status,  tglfaktur, supplier, nofaktur) values ('$id','$datetime','$stok','1','$datetime','Stok Opname','Stok Opname')");
                                        $crePro->execute();
                                        if($crePro && $upPro){                                      
                                            echo '<script type="text/javascript">
                                                setTimeout(function() {
                                                    swal({
                                                        title: "Sukses!",
                                                        text: "Berhasil Update Stok!",
                                                        type: "success"
                                                    }, function() {
                                                        window.close();
                                                    });
                                                }, 100);
                                            </script>';
                                        }else{
                                            echo '<script type="text/javascript">'; 
                                            echo 'alert("Gagal menambahkan");'; 
                                            echo 'window.location.href = "../../unit.php";';
                                            echo '</script>';
                                        }
                                        
                                    }elseif($stok > $upstok){
                                        $stok = $stok - $upstok;
                                        $upPro = $con->prepare("UPDATE products set quantity = '$upstok' where id = '$id'");
                                        $upPro->execute();

                                        $datetime = date("Y-m-d H:i:s");
                                        $crePro = $con->prepare("INSERT INTO transaksi (id_barang, id_unit, waktu, qty, status, catatan) values ('$id','5','$datetime','$stok','2','Stok Opname')");
                                        $crePro->execute();
                                        if($crePro && $upPro){                                      
                                            echo '<script type="text/javascript">
                                                setTimeout(function() {
                                                    swal({
                                                        title: "Sukses!",
                                                        text: "Berhasil Update Stok!",
                                                        type: "success"
                                                    }, function() {
                                                        window.close();
                                                    });
                                                }, 100);
                                            </script>';
                                        }else{
                                            echo '<script type="text/javascript">'; 
                                            echo 'alert("Gagal menambahkan");'; 
                                            echo 'window.location.href = "../../unit.php";';
                                            echo '</script>';
                                        }

                                        echo $stok;
                                    }else{
                                        echo "nothing";
                                    }
                                }else{
                                    
                                }
						?>
                <div class="pure-g">
                    <div class="pure-u-1 pure-u-md-1-1">
                        <div class="column-block" ng-app="liveApp" ng-controller="liveController" ng-init="fetchData()">
						<form action="updatestok.php?id=<?= $id; ?>&s=edit" method="post" novalidate autocomplete="off" class="pure-form pure-form-stacked">
							<fieldset>
                            <input name="idpro" type="hidden" value="<?php echo $_GET['id']; ?>">

                                <input name="id" type="hidden" value="<?php echo $produk['id']; ?>">
								<label>Nama Produk</label>
								<input disabled name="nama" type="text" placeholder="Nama" class="pure-input-1" value="<?php echo $produk['name']; ?>">

                                <label>Stok Sekarang</label>
                                <input name="now" type="hidden" value="<?php echo $produk['quantity']; ?>">
								<input disabled name="now" type="number" placeholder="Stok saat ini" class="pure-input-1" value="<?php echo $produk['quantity']; ?>">

                                <label>Update Stok</label>
								<input name="update" required type="number" placeholder="Stok Sekarang" class="pure-input-1" value="">
								<button type="submit" class="pure-button button-success">Simpan</button>
							</fieldset>
						</form>
                        </div>
                    </div>
                  </div>


                <?php require_once "../../footer.php"; ?>
