<html lang="en">
<head>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
    integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A layout example that shows off a blog page with a list of posts.">
    <title>RSOP</title>
    <link rel="stylesheet" href="../../assets/css/pure-min.css">
    <link rel="stylesheet" href="../../assets/css/pure-responsive-min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
</head>
<body>
<?php 
$con = new pdo('mysql:host=localhost;dbname=inventory', 'root', '');
// menghubungkan dengan library excel reader
include "excel_reader2.php";
?>
 
<?php
// upload file xls
$target = basename($_FILES['filestok']['name']) ;
move_uploaded_file($_FILES['filestok']['tmp_name'], $target);
 
// beri permisi agar file xls dapat di baca
chmod($_FILES['filestok']['name'],0777);
 
// mengambil isi file xls
$data = new Spreadsheet_Excel_Reader($_FILES['filestok']['name'],false);
// menghitung jumlah baris data yang ada
$jumlah_baris = $data->rowcount($sheet_index=0);
 
// jumlah default data yang berhasil di import
$berhasil = 0;

for ($i=4; $i<=$jumlah_baris; $i++){
 
	// menangkap data dan memasukkan ke variabel sesuai dengan kolumnya masing-masing
    $id = $data->val($i, 2);
    $stok = $data->val($i,5);
	$upstok = $data->val($i, 6);
 
	if($id != "" && $upstok != ""){
		if($upstok > $stok){
            $stok = $upstok-$stok;
            $upPro = $con->prepare("UPDATE products set quantity = '$upstok' where id = '$id'");
            $upPro->execute();

            $datetime = date("Y-m-d H:i:s");
            $crePro = $con->prepare("INSERT INTO transaksi (id_barang, waktu, qty, status,  tglfaktur, supplier, nofaktur) values ('$id','$datetime','$stok','1','$datetime','Stok Opname','Stok Opname')");
            $crePro->execute();
            
            
        }elseif($stok > $upstok){
            $stok = $stok - $upstok;
            $upPro = $con->prepare("UPDATE products set quantity = '$upstok' where id = '$id'");
            $upPro->execute();

            $datetime = date("Y-m-d H:i:s");
            $crePro = $con->prepare("INSERT INTO transaksi (id_barang, id_unit, waktu, qty, status, catatan) values ('$id','5','$datetime','$stok','2','Stok Opname')");
            $crePro->execute();

            
            
        }else{
            
        }
	}
}                                   
    
 
// hapus kembali file .xls yang di upload tadi
unlink($_FILES['filestok']['name']);

echo '<script type="text/javascript">
        setTimeout(function() {
            swal({
                title: "Sukses!",
                text: "Berhasil Update Stok!",
                type: "success"
            }, function() {
                window.location.href = "../../stokopname.php"
            });
        }, 100);
    </script>';
 
// alihkan halaman ke index.php
// header("location:index.php?berhasil=$berhasil");
?>
</body>
</html>