<html>
<head>
	<title>Export Data Ke Excel Dengan PHP - www.malasngoding.com</title>
</head>
<body>
	<style type="text/css">
	body{
		font-family: sans-serif;
	}
	table{
		margin: 20px auto;
		border-collapse: collapse;
	}
	table th,
	table td{
		border: 1px solid #3c3c3c;
		padding: 3px 8px;
 
	}
	a{
		background: blue;
		color: #fff;
		padding: 8px 10px;
		text-decoration: none;
		border-radius: 2px;
	}
	</style>
 
	<?php
    
    $con = new pdo('mysql:host=localhost;dbname=inventory', 'root', '');

    if(empty($_GET['id'])){
        header('location:https://google.com');
    }else{
        $id = $_GET['id'];
    }

    $query1 = $con->prepare("SELECT * FROM products INNER JOIN categories on products.categorie_id = categories.id where products.categorie_id = '$id'");
    $query1->execute();
    $rows = $query1->fetchAll();
    $no = 1;

    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Data Stokopname ".$rows[0][10].".xls");

	?>
 
	<center>
		<h1>Stok Opname <?= $rows[0][10] ?> <br/></h1>
	</center>
 
	<table border="1">
        <thead>
            <tr style="background:#429eea;color:#fff;">
                <th>#</th>
                <th>Id Barang</th>
                <th>Nama Barang</th>
                <th>Kategori</th>
                <th>Stok</th>
                <th>Hasil Temuan</th>
            </tr>
        </thead>
        <?php
            foreach($rows as $row){

        ?>
        <tr>
            <td><?= $no++; ?></td>
            <td><?= $row[0]; ?></td>
            <td><?= $row[1]; ?></td>
            <td><?= $row[10] ?></td>
            <td><?= $row['quantity'] ?></td>
            <td></td>
        </tr>
            <?php } ?>
	</table>
</body>
</html>