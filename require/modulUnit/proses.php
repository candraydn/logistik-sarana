<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Process Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
    <script src="main.js"></script>
</head>
<body>

<?php
require_once("koneksi.php");

if(!empty($_GET['s'])){
    $status = $_GET['s'];
    if($status == "add"){
        if(!empty($_POST['add'])){
            $x = $_POST['add'];
            $query = mysqli_query($db, "INSERT into unit (nama_unit) values ('$x')");
            if($query){
                echo '<script type="text/javascript">
                    setTimeout(function() {
                        swal({
                            title: "Sukses!",
                            text: "Berhasil menambahkan Unit Kerja!",
                            type: "success"
                        }, function() {
                            window.location = "../../unit.php";
                        });
                    }, 100);
                </script>';
            }else{
                echo '<script type="text/javascript">'; 
                echo 'alert("Gagal menambahkan");'; 
                echo 'window.location.href = "../../unit.php";';
                echo '</script>';
            }
        }
    }elseif($status == "del" && isset($_GET['id'])){
        $id = $_GET['id'];
        $query = mysqli_query($db, "DELETE FROM unit where id_unit = '$id'");
            if($query){
                echo '<script type="text/javascript">
                    setTimeout(function() {
                        swal({
                            title: "Sukses!",
                            text: "Berhasil menghapus Unit Kerja!",
                            type: "success"
                        }, function() {
                            window.location = "../../unit.php";
                        });
                    }, 100);
                </script>';
            }else{
                echo '<script type="text/javascript">'; 
                echo 'alert("Gagal menghapus");'; 
                echo 'window.location.href = "../../unit.php";';
                echo '</script>';
            }
    }elseif($status == "edit" && isset($_GET['id'])){
        $id = $_GET['id'];
        $x = $_POST['name'];
        $query = mysqli_query($db, "UPDATE unit set nama_unit = '$x' where id_unit = '$id'");
            if($query){
                echo '<script type="text/javascript">'; 
                echo 'alert("Sukses mengedit");'; 
                echo 'window.location.href = "../../unit.php";';
                echo '</script>';
            }else{
                echo '<script type="text/javascript">'; 
                echo 'alert("Gagal mengedit");'; 
                echo 'window.location.href = "../../unit.php";';
                echo '</script>';
            }
    }
}
?>
    
    </body>
</html>