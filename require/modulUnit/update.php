<?php
  session_start();

  require_once "../authCookieSessionValidate.php";

  if(!$isLoggedIn) {
      header("Location: ./");
  }
?>

<!doctype html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
    integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>  
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A layout example that shows off a blog page with a list of posts.">
    <title>RSOP</title>
    <link rel="stylesheet" href="../../assets/css/pure-min.css">
    <link rel="stylesheet" href="../../assets/css/pure-responsive-min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
</head>
<body>
    <div id="layout" class="pure-g">
        <div class="sidebar pure-u-1 pure-u-md-3-24">
            <div id="menu">
                <div class="pure-menu">
                    <p class="pure-menu-heading">
                        RSOP
                        <a href="require/logout.php" class="pure-button button-xxsmall">OUT &raquo;</a>
                    </p>
                    <ul class="pure-menu-list">
                        <li>
                            <a href="../../dashboard.php" class="pure-menu-link"><i class="fas fa-home"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="../../stokbarang.php" class="pure-menu-link"><i class="fas fa-bars"></i> Stok Barang</a>
                        </li>
                        <li>
                            <a href="../../kategori.php" class="pure-menu-link"><i class="fas fa-bookmark"></i> Kategori</a>
                        </li>
                        <li>
                            <a href="../../unit.php" class="pure-menu-link"><i class="fas fa-clone"></i> Unit</a>
                        </li>
                        <li>
                            <a href="../../barangmasuk.php" class="pure-menu-link"><i class="fas fa-chevron-circle-right"></i> Barang Masuk</a>
                        </li>
                        <li>
                            <a href="../../barangkeluar.php" class="pure-menu-link"><i class="fas fa-chevron-circle-left"></i> Barang Keluar</a>
                        </li>
                        <li>
                            <a href="../../laporan.php" class="pure-menu-link"><i class="fas fa-book"></i> Laporan</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content pure-u-1 pure-u-md-21-24">
            <div class="header-small">

                <div class="items"> 
                    <h1 class="subhead">Edit Unit</h1>
                </div>

                <div class="pure-g">
                    <div class="pure-u-1 pure-u-md-1-1">
                        <div class="column-block">
						
                        <?php
                            require_once("koneksi.php");
                            if(!empty($_GET['id'])){
                                $id = $_GET['id'];
                                $query = mysqli_query($db,"SELECT * FROM unit where id_unit='$id'");
                                $hasil = mysqli_fetch_array($query);
                            }else{
                                header('location:https://google.com');
                            }

                        ?>
                        <form action="proses.php?s=edit&id=<?= $id ?>" method="post" class="pure-form pure-form-stacked">
                        <table>
                        <tr>
                            <td><label>Kategori</label>
                            <input name="name" type="text" value="<?= $hasil['nama_unit'] ?>"> <input id="button" type="submit" class="pure-button button-success" name="transaksi" value="Edit" /></td>
                        </tr>
                        </table>
                            
						</form>
                        </div>
                    </div>
                  </div>

                <?php require_once "../../footer.php"; ?>
