<table class="pure-table pure-table-striped">
    <thead>
        <tr style="background:#429eea;color:#fff;">
            <th>id</th>
            <th>Nama Barang</th>
            <th>Kategori</th>
            <th>Harga Beli</th>
            <th>Opsi</th>
        </tr>
    </thead>
    <tbody>
    
<?php
function rupiah($angka){
	
	$hasil_rupiah = "Rp. " . number_format($angka,0,',','.');
	return $hasil_rupiah;
 
}
    include_once "koneksi.php";
    
    if(isset($_POST["query"]))
    {

    $q = $_POST["query"];
        
    $query = mysqli_query($db,"SELECT
    products.id as idproduk,
    products.`name`,
    products.quantity,
    products.categorie_id,
    categories.id,
    categories.`name` AS kategori,
    products.buy_price,
    products.sale_price
    FROM
    products
    INNER JOIN categories ON products.categorie_id = categories.id 
    WHERE products.name LIKE '%" . $q . "%' LIMIT 15");

    }
    else
    {
    
    $query = mysqli_query($db,"SELECT
    products.id as idproduk,
    products.`name`,
    products.quantity,
    products.categorie_id,
    categories.id,
    categories.`name` AS kategori,
    products.buy_price,
    products.sale_price
    FROM
    products
    INNER JOIN categories ON products.categorie_id = categories.id LIMIT 15");

    }
    while($hasil = mysqli_fetch_array($query)){
?>

        <tr onclick="javascript:showRow(this);">
            <td><?php echo $hasil['idproduk']; ?></td>
            <td><?php echo $hasil['name']; ?></td>
            <td><?php echo $hasil['kategori']; ?></td>
            <td><?php echo rupiah($hasil['buy_price']); ?></td>
            <td><button class="pure-button button-small button-success">Pilih</button></td>
        </tr>
<?php
    }
?>
    </tbody>
</table>