<?php
require_once "header.php";

function rupiah($angka){
	
	$hasil_rupiah = "Rp " . number_format($angka,0,',','.');
	return $hasil_rupiah;
 
}
 ?>

<div class="content pure-u-1 pure-u-md-21-24">
	<div class="header-small">

		<div class="items">
			<h1 class="subhead">Stok Barang <a href="require/modulProduk/tambahProduk.php" class="pure-button button-small button-success">Tambah
					Barang</a></h1>
		</div>

		<div class="pure-g">
			<div class="pure-u-1 pure-u-md-1-1">
				<div class="column-block" ng-app="liveApp" ng-controller="liveController" ng-init="fetchData()">

					<form name="testform" ng-submit="insertData()" novalidate autocomplete="off" class="pure-form pure-form-stacked">
						<table class="pure-table pure-table-bordered">
							<thead>
								<tr>
									<th>#</th>
									<th>Nama Barang Barang</th>
									<th>Kategori</th>
									<th>Stok</th>
									<th>Harga Beli</th>
									<th width="20%">Option</th>
								</tr>
							</thead>

							<tbody>

								<tr>
									<form action="" name="cari" method="POST">
										<td colspan="5"><input type="text" class="pure-input-1" placeholder="Cari Barang" /></td>
										<td>
											<button type="submit" ng-disabled="testform.$invalid" class="pure-button button-small button-secondary">Cari</a>
										</td>
									</form>
								</tr>
								<?php
								include_once "require/modulProduk/koneksi.php";
								$halaman = 20;
  								$page = isset($_GET["halaman"]) ? (int)$_GET["halaman"] : 1;
 								$mulai = ($page>1) ? ($page * $halaman) - $halaman : 0;
								$no = $mulai;
								$queryjmlh = mysqli_query($db, "SELECT
								* FROM products
								INNER JOIN categories ON products.categorie_id = categories.id");
								$jmlh = mysqli_num_rows($queryjmlh);
								$pages = ceil($jmlh/$halaman);    
								$sql = "SELECT
										products.id as idproduk,
										products.`name`,
										products.satuan,
										products.quantity,
										products.categorie_id,
										categories.id,
										categories.`name` AS kategori,
										products.buy_price,
										products.sale_price
										FROM
										products
										INNER JOIN categories ON products.categorie_id = categories.id 
										ORDER BY products.name ASC
										LIMIT $mulai, $halaman
										";
								$query = mysqli_query($db, $sql);
								while($produk = mysqli_fetch_array($query)){
									$no++;
									$id = $produk['idproduk'];
							  ?>
								<tr>
									<td>
										<?php echo $no; ?>
									</td>
									<td>
										<?php echo $produk['name']; ?>
									</td>
									<td>
										<?php echo $produk['kategori']; ?>
									</td>
									<td>
										<?php echo $produk['quantity']; echo " ".$produk['satuan']; ?>
									</td>
									<td>
										<?php echo rupiah($produk['buy_price']); ?>
									</td>
									<td>
										<a class="pure-button button-small button-success" href="require/modulProduk/editProduk.php?id=<?php echo $id; ?>">Edit</a>
										<a onclick="return confirm('Yakin mau menghapus produk?')" class="pure-button button-small button-warning"
										 href="require/modulProduk/prosesproduk.php?id=<?php echo $id; ?>&status=delete">Delete</button>
									</td>
								</tr>
								<?php
								}
							  ?>
							</tbody>
						</table>
				</div>
				<B>Halaman : </B>
				<?php 
						  for($i=1; $i <= $pages; $i++){ 
							  
							  ?>
				<a href="?halaman=<?php echo $i; ?>" class="pure-button">
					<?php echo $i; ?></a>
				<?php } ?>
			</div>
		</div>


		<?php require_once "footer.php"; ?>
