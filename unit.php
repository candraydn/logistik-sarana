<?php
require_once "header.php";
require_once "require/barangmasuk/koneksi.php";
error_reporting(E_ALL);
function rupiah($angka){
	
	$hasil_rupiah = "Rp " . number_format($angka,0,',','.');
	return $hasil_rupiah;
 
}
?>
<div class="content pure-u-1 pure-u-md-21-24">
    <div class="header-small">

        <div class="items">
            <h1 class="subhead">Unit</h1>
        </div>

        <div class="pure-g">
            <div class="pure-u-1 pure-u-md-1-1">
                <div class="column-block">
                    <form name="testform" action="require/modulUnit/proses.php?s=add" method="post" novalidate autocomplete="off"
                        class="pure-form pure-form-stacked">
                        <table class="pure-table pure-table-bordered">
                            <thead>
                                <tr style="background:#fdffaa">
                                    <th>Unit Kerja</th>
                                    <th width="20%">Option</th>
                                </tr>
                            </thead>

                            <tbody>

                                <tr>
                                    <td><input type="text" class="pure-input-1"
                                            placeholder="Unit Baru" name="add" required/></td>
                                    <td>
                                        <button type="submit"
                                            class="pure-button button-small button-secondary">Tambah</a>
                                    </td>
                                </tr>
                                </form>
                                <?php
                                $query = mysqli_query($db,"SELECT * from unit") or die(mysql_error());
                                while ($hasil = mysqli_fetch_array($query)) {
                              ?>
                              
                                <tr>
                                    <td><?= $hasil['nama_unit']; ?></td>
                                    <td>
                                        <a href="require/modulUnit/update.php?id=<?= $hasil['id_unit'] ?>"
                                            class="pure-button button-small button-success">Edit</a>
                                        <a href="require/modulUnit/proses.php?s=del&id=<?= $hasil['id_unit'] ?>"
                                            onclick="return confirm('Akan menghapus <?= $hasil['nama_unit']; ?> ?')" class="pure-button button-small button-warning">Delete</a>
                                    </td>
                                </tr>
                                <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
        <?php require_once "footer.php"; ?>