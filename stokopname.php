<?php
require_once "header.php";
 ?>
<div class="content pure-u-1 pure-u-md-21-24">
    <?php
        $con = new pdo('mysql:host=localhost;dbname=inventory', 'root', '');

        $query = $con->prepare("SELECT * FROM categories");
        $query->execute();
        $result = $query->fetchAll();

        if(isset($_GET['kategori'])){
            $id = $_GET['kategori'];
        }
    ?>
    <div class="header-small">

        <div class="items">
            <h1 class="subhead">Stok Opname </h1>
        </div>
        <div class="pure-g">
            <div class="pure-u-1 pure-u-md-1-1">
                <div class="input-group">
                    <form action="" method="get" enctype="multipart/form-data">
                    <select name='kategori'>
                        <?php
                            foreach($result as $row){

                                if(isset($_GET['kategori'])){
                                    if($_GET['kategori'] == $row['id']){
                                        echo "<option selected value=".$row['id'].">".$row['name']."</option>";
                                    }else{
                                        echo "<option value=".$row['id'].">".$row['name']."</option>";
                                    }
                                    ?>
                                                
                                    <?php
                                }else{
                                ?>
                        <option value='<?= $row['id'] ?>'><?= $row['name'] ?></option>
                        <?php
                                }
                            }
                        ?>
                    </select>
                    <input type="submit" class="pure-button button-small button-secondary" value="Cari">
                    </form><br>
                    <a class="pure-button button-small button-success" href="require/stokopname/xlsexport.php?id=<?= $id ?>">Export</a>
                    <br><br>
                    <span>Import File</span>
                    <form id="form" action="require/stokopname/xls-upload.php" enctype="multipart/form-data" method="post">
                        <input accept="application/vnd.ms-excel" type="file" name="filestok" value="Import" class="pure-button button-small button-secondary" id=file>
                    </form>
                </div>
                <div id="view" style="margin-top:30px">
                <table class="pure-table pure-table-striped">
                        <thead>
                            <tr style="background:#429eea;color:#fff;">
                                <th>#</th>
                                <th>Nama Barang</th>
                                <th>Kategori</th>
                                <th>Stok</th>
                                <th>Update Stok</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if(isset($_GET['kategori'])){
                                $id = $_GET['kategori'];
                                $query1 = $con->prepare("SELECT * FROM products INNER JOIN categories on products.categorie_id = categories.id where products.categorie_id = '$id'");
                                $query1->execute();
                                $rows = $query1->fetchAll();
                                $no = 1;
                                foreach($rows as $row){

                        
                            ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $row[1]; ?></td>
                                <td><?= $row[10] ?></td>
                                <td><?= $row['quantity'] ?></td>
                                <td><a target="_blank" class="pure-button button-success" href="require/stokopname/updatestok.php?id=<?php echo $row['0']; ?>&s=edit"><i class="fas fa-edit"></i></a></td>
                            </tr>
                            <?php
                                }
                            }
                            ?>
                        </tbody>
                </table>
                </div>
            </div>
        </div>

    </div>

    <!-- Load File jquery.min.js yang ada difolder js -->
    <script src="assets/js/jquery.min.js"></script>

    <!-- Load File bootstrap.min.js yang ada difolder js -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Load file ajax.js yang ada di folder js -->
    <script src="assets/js/ajax.js"></script>

    <script type="text/javascript">
        document.getElementById("file").onchange = function() {
            document.getElementById("form").submit();
        };
    </script>
    <?php include_once "footer.php"; ?>