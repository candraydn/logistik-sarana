<?php
require_once "header.php";
require_once "require/barangmasuk/koneksi.php";
error_reporting(E_ALL);
function rupiah($angka){
	
	$hasil_rupiah = "Rp " . number_format($angka,0,',','.');
	return $hasil_rupiah;
 
}
 ?>
<div class="content pure-u-1 pure-u-md-21-24">
    <div class="header-small">

        <div class="items">
            <h1 class="subhead">Laporan Transaksi</h1>
        </div>
        <div class="pure-g">
            <div class="pure-u-1 pure-u-md-1-1">
                <form action="" method="post" class="pure-form">
                <table>
                    <tr>
                        <td width="30%">Tanggal</td>
                        <td width="20%">Status</td>
                        <td width="20%">Unit</td>
                        <td width="10%">Keyword barang</td>
                        <td width="20%">Tampilan</td>
                    </tr>
                    <tr>
                        <td>
                            <i class="far fa-calendar-plus"></i> <input autocomplete="off" style="background-color:#f0ff6b; color:#000;" type="text" name="datefilter" value="" placeholder="Periode Tanggal" onfocus="nonAktif()" onblur="aktif()"/>
                        </td>
                        <td>
                            <select style="background-color:#f0ff6b; color:#000;" id="state" class="" name="jenis">
                                <option value="1">Barang Masuk</option>
                                <option value="2">Barang Keluar</option>
                            </select>
                        </td>
                        <td>
                        <select style="background-color:#f0ff6b; color:#000;" id="state" name="unitForm">
                            <option value="">--Pilih Semua--</option>
                            <?php
                                $queryUnit = mysqli_query($db,"Select * from unit");
                                while($hasilUnit = mysqli_fetch_array($queryUnit)){
                            ?>
                            <option value="<?= $hasilUnit['id_unit'] ?>"><?= $hasilUnit['nama_unit'] ?></option>
                            <?php
                                }
                            ?>
                        </select>
                        </td>
                        <td>
                            <input style="background-color:#f0ff6b; color:#000;" type="text" name="keyword" placeholder="Keyword ..." />
                        </td>
                        <td>
                        <select style="background-color:#f0ff6b; color:#000;" id="state" class="" name="unitForm">
                            <option value="DESC">Terbaru</option>
                            <option value="ASC">Terlama</option>
                        </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"><br>    
                            <input type="submit" class="pure-button button-success" value="Cari">
<!--                            <input type="submit" class="pure-button button-warning" value="Print">-->
                        </td>
                    </tr>
                </table>
                    <!-- <label class="pure-radio">
                        Jenis Transaksi: 
                        <select name="jenis" class="pure-input-1-1">
                            <option value="1">Barang Masuk</option>
                            <option value="2">Barang Keluar</option>
                        </select>
                        | Tanggal:
                        <input type="date" name="tgl" required>
                        <input type="submit" class="pure-button button-small button-success" value="Cari">
                    </label> -->
                </form>
                <br><br>
                <button onclick="printContent('printarea')">Cetak Laporan</button><br><br>
                <div id="printarea">
                <?php
                    if(isset($_POST['jenis'])){
                        $keyword = $_POST['keyword'];
                        $jenis = $_POST['jenis'];
                        $daterange = $_POST['datefilter'];
                        $daterange = explode('-',$daterange);
                        $daterangeX = explode('/',$daterange[0]);
                        $daterangeY = explode('/',$daterange[1]);

                        $tglDari = $daterangeX[2];
                        $tglDari .= '-'.$daterangeX[0];
                        $tglDari .= '-'.$daterangeX[1];
                        $tglDari = preg_replace('/\s+/', '', $tglDari);

                        $tglSampai = $daterangeY[2];
                        $tglSampai .= '-'.$daterangeY[0];
                        $tglSampai .= '-'.$daterangeY[1];
                        $tglSampai = preg_replace('/\s+/', '', $tglSampai);
                        if($jenis == 2){
                ?>
                Pencarian <b>barang keluar</b> antara tanggal <b><?= $tglDari ?> dan <?= $tglSampai ?></b><br><br>
                <table class="pure-table pure-table-striped">
                        <thead>
                            <tr style="background:#429eea;color:#fff;">
                                <th>Tgl Keluar</th>
                                <th>Nama Barang</th>
                                <th>Jumlah</th>
                                <th>Catatan</th>
                                <!-- <th>Opsi</th> -->
                            </tr>
                        </thead>

                        <tbody>
                <?php
                        $query = mysqli_query($db,"SELECT
                        transaksi.id_transaksi,
                        transaksi.waktu,
                        transaksi.catatan,
                        transaksi.id_barang as idproduk,
                        transaksi.qty as qtyi,
                        transaksi.`status`,
                        transaksi.buy_price,
                        transaksi.sale_price,
                        products.id,
                        products.`name` as namapro,
                        products.quantity,
                        products.buy_price as harga,
                        products.sale_price,
                        products.satuan,
                        products.categorie_id,
                        products.media_id,
                        products.date,
                        categories.id,
                        categories.`name` as kategori
                        FROM
                        transaksi
                        INNER JOIN products ON transaksi.id_barang = products.id
                        INNER JOIN categories ON products.categorie_id = categories.id
                        WHERE
                        products.`name` like '%$keyword%' AND
                        transaksi.`status` = 2 AND 
                        date_format(transaksi.waktu, '%Y-%m-%d') BETWEEN date_format('$tglDari', '%Y-%m-%d') AND date_format('$tglSampai', '%Y-%m-%d')
                        ORDER BY transaksi.waktu ASC");
                        $jumlah = 0;
                        $total = 0;
                        while($hasilTrx = mysqli_fetch_array($query)){
                            $totalharga = $hasilTrx['harga']*$hasilTrx['qtyi'];
                            ?>
                                <tr>
                                    <td><?php echo date("d-M-Y", strtotime($hasilTrx['waktu'])); ?></td>
                                    <td><?php echo $hasilTrx['namapro']; ?></td>
                                    <td><?php echo $hasilTrx['qtyi']; echo " ".$hasilTrx['satuan']; ?></td>
                                    <td><?php echo $hasilTrx['catatan']; ?></td>
                                    <!-- <td><a class="pure-button pure-button-primary" href="require/barangkeluar/cetakbbk.php?tgl=<?php echo $hasilTrx['waktu']; ?>&catatan=<?php echo $hasilTrx['catatan']; ?>" target="_blank" id="cetak"><i class="fas fa-print"></i></a>
                                    <a class="pure-button button-success" href="require/barangkeluar/prosesbarangkeluar.php?id=<?php echo $hasilTrx['id_transaksi']; ?>&s=edit" id="cetak"><i class="fas fa-edit"></i></a>
                                    <a onclick="return confirm('Yakin akan menghapus?');" class="pure-button button-error" href="require/barangkeluar/prosesbarangkeluar.php?id=<?php echo $hasilTrx['id_transaksi']; ?>&s=del"><i class="fas fa-trash-alt"></i></a></td> -->
                                </tr>
                            <?php
                            $jumlah += $hasilTrx['qtyi'];
                            $total += $totalharga;
                            }
                            ?>
                                <tr style="background-color:#f0ff6b">
                                    <td colspan="2" style="text-align:right;">Total</td>
                                    <td><?= $jumlah ?></td>
                                    <td><?= rupiah($total); ?></td>
                                </tr>
                            </tbody>
                </table>
                    <?php
                        }else{
                            ?>
                            Pencarian <b>barang masuk</b> antara tanggal <b><?= $tglDari ?> dan <?= $tglSampai ?></b><br><br>
                <table class="pure-table pure-table-striped">
                        <thead>
                            <tr style="background:#429eea;color:#fff;">
                                <th>Tgl Faktur</th>
                                <th>Supplier</th>
                                <th>No Faktur</th>
                                <th>Nama Barang</th>
                                <th>Jumlah</th>
                                <th>Total Harga</th>
                                <!-- <th>Opsi</th> -->
                            </tr>
                        </thead>
                        <tbody>
                <?php
                        $query = mysqli_query($db,"SELECT
                        transaksi.id_transaksi,
                        transaksi.waktu,
                        transaksi.id_barang as idproduk,
                        transaksi.qty as qtyi,
                        transaksi.`status`,
                        transaksi.buy_price as harga,
                        transaksi.sale_price,
                        transaksi.tglfaktur,
                        transaksi.nofaktur,
                        transaksi.supplier,
                        products.id,
                        products.`name` as namapro,
                        products.quantity,
                        products.buy_price,
                        products.sale_price,
                        products.categorie_id,
                        products.satuan,
                        products.media_id,
                        products.date,
                        categories.id,
                        categories.`name` as kategori
                        FROM
                        transaksi
                        INNER JOIN products ON transaksi.id_barang = products.id
                        INNER JOIN categories ON products.categorie_id = categories.id
                        WHERE
                        products.`name` like '%$keyword%' AND
                        transaksi.`status` = 1 and 
                        date_format(transaksi.waktu, '%Y-%m-%d') BETWEEN date_format('$tglDari', '%Y-%m-%d') AND date_format('$tglSampai', '%Y-%m-%d')
                        ORDER BY transaksi.tglfaktur ASC");
                        $jumlah = 0;
                        $total = 0;
                        while($hasilTrx = mysqli_fetch_array($query)){
                            $totalharga = $hasilTrx['harga']*$hasilTrx['qtyi'];
                            ?>
                                    <tr>
                                        <td><?php echo date("d-M-Y", strtotime($hasilTrx['tglfaktur'])); ?></td>
                                        <td><?php echo $hasilTrx['supplier']; ?></td>
                                        <td><?php echo $hasilTrx['nofaktur']; ?></td>
                                        <td><?php echo $hasilTrx['namapro']; ?></td>
                                        <td><?php echo $hasilTrx['qtyi']; echo " ".$hasilTrx['satuan']; ?></td>
                                        <td><?php echo rupiah($totalharga); ?></td>
                                        <!-- <td><a class="pure-button pure-button-primary" href="require/barangmasuk/cetakbbk.php?tgl=<?php echo $hasilTrx['tglfaktur']; ?>&nofaktur=<?= $hasilTrx['nofaktur'] ?>" target="_blank" id="cetak"><i class="fas fa-print"></i></a>
                                        <a class="pure-button button-success" href="require/barangmasuk/prosesbarangmasuk.php?id=<?php echo $hasilTrx['id_transaksi']; ?>&s=edit" id="cetak"><i class="fas fa-edit"></i></a>
                                        <a onclick="return confirm('Yakin akan menghapus?');" class="pure-button button-error" href="require/barangmasuk/prosesbarangkeluar.php?id=<?php echo $hasilTrx['id_transaksi']; ?>&s=del"><i class="fas fa-trash-alt"></i></a></td> -->
                                    </tr>
                            <?php
                                $jumlah += $hasilTrx['qtyi'];
                                $total += $totalharga;
                            }
                            ?> 
                                    <tr>
                                        <td colspan="4" style="text-align:right">Total</td>
                                        <td><?= $jumlah; ?></td>
                                        <td><?= rupiah($total); ?></td>
                                    </tr>
                            </tbody>
                </table>
                    <?php
                        }
                    }else{
                        echo "Pilih jenis transaksi dan tanggal dulu";
                    }

                ?>
                </div>
                    
            </div>
        </div>
</div>
<script type="text/javascript">
$(function() {

  $('input[name="datefilter"]').daterangepicker({
      autoUpdateInput: false,
      locale: {
          cancelLabel: 'Clear'
      }
  });

  $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
  });

  $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });

});
function nonAktif()
{
 document.onkeydown = function (e)
 {
  return false;
 }
}
function aktif()
{
 document.onkeydown = function (e)
 {
  return true;
 }
}
</script>
<script>
    function printContent(el){
        var restorepage = document.body.innerHTML;
        var printcontent = document.getElementById(el).innerHTML;
        document.body.innerHTML = printcontent;
        window.print();
        document.body.innerHTML = restorepage;
    }
</script>
<?php include_once "footer.php"; ?>